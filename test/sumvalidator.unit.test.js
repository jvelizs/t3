const sumvalidator=require('../src/sumvalidator')
//const sinon= require('sinon')

/**
 * 1- Arreglo donde se encuentre la suma
 * 2- Arreglo donde no se encuentre la suma
 * 3- Arreglo donde se enceuntre la suma pero dentro hay valores como string
 * 4- Arreglo vacio
 * 5- Arreglo que contiene palabras, y se encuentra la suma
 * 6- 
 */
describe('Sumvalidator Test', () => {
    it('Arreglo donde se encuentre la suma',()=>{
        const array= sumvalidator.twoNumberSum([1, 3, 6, -5, 2], 9)
        expect(array).toStrictEqual([3,6])
    })
    it('Arreglo donde no se encuentre la suma',()=>{
        const array= sumvalidator.twoNumberSum([1, 3, 3, -5, 2], 9)
        expect(array).toBe(undefined)
    })
    it('Arreglo donde se enceuntre la suma pero dentro hay valores como string',()=>{
        const array= sumvalidator.twoNumberSum([1, '3', '6', -5, 2], 9)
        expect(array).toStrictEqual([3,6])
    })
    it('Arreglo vacio',()=>{
        const array= sumvalidator.twoNumberSum([], 9)
        expect(array).toBe(undefined)
    })
    it('Arreglo que contiene palabras, y se encuentra la suma',()=>{
        const array= sumvalidator.twoNumberSum(['hola','buena','loco',6, 3], 9)
        expect(array).toStrictEqual([6,3])
    })
})