const sumvalidator = require('../t3/src/sumvalidator')
const express = require('express')
const bodyParse= require('body-parser')
const json = require('body-parser/lib/types/json')

const app= express()
module.exports = app
app.use(bodyParse.json())
app.use(bodyParse.urlencoded({
        extended: true
}))
app.post('/sumvalidator/:targetSum', (request, response)=> {
    if(request.body.array){
        if(sumvalidator.twoNumberSum(request.body.array, request.params.targetSum)!=undefined){
            response.status(200).json({status: 200, message: 'Se encontraron coincidencias', data: sumvalidator.twoNumberSum(request.body.array, request.params.targetSum)})
        }
        if(sumvalidator.twoNumberSum(request.body.array, request.params.targetSum)==undefined){
            response.status(200).json({status: 200, message: 'No se encontraron coincidencias', data: []})
        }
    }
    else{
        response.status(500).json({status: 500, message:  "Hubo un error al intentar procesar su solicitud", data: []})
    }

})
app.listen(3001,function(){
    console.log('Server is running in port 3001')
})